package com.walkthespot.app;

import java.util.Vector;

public class ImageCategory {
	public Vector<Integer> pics = new Vector<Integer>();
	public String name = "";
	
	ImageCategory(String name) {
		this.name = name;
	}
	
	public void add(Integer pic) {
		pics.add(pic);
	}
}
