package com.walkthespot.app;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class WalkthroughActivity extends Activity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        int layoutResId = getIntent().getExtras().getInt("layout", 0);
        setContentView(layoutResId);
        
        Typeface face = Typeface.createFromAsset(getAssets(), "MuseoSans_500.otf");
        
        TextView tx1 = (TextView)findViewById(R.id.tx1);
        if (tx1 != null)
        	tx1.setTypeface(face);
        
        TextView tx2 = (TextView)findViewById(R.id.tx2);
        if (tx2 != null)
        	tx2.setTypeface(face);
        
        TextView tx3 = (TextView)findViewById(R.id.tx3);
        if (tx3 != null)
        	tx3.setTypeface(face);
        
        /*TextView tx4 = (TextView)findViewById(R.id.tx4);
        if (tx4 != null)
        	tx4.setTypeface(face);*/
        
        RelativeLayout ll = (RelativeLayout)findViewById(R.id.walk_layout);
        ll.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
        
    }
	
	
}
