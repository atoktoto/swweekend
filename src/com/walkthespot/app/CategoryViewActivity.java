package com.walkthespot.app;

import java.util.Vector;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CompoundButton;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ToggleButton;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;

public class CategoryViewActivity extends Activity {
    /** Called when the activity is first created. */
	ToggleButton modeToggle;
	Button mapButton;
	Boolean editMode = false;
	Vector<ImageView> images = new Vector<ImageView>();
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.picture_view);
       
        // WALKTHROUGH
        Intent intent = new Intent(this, WalkthroughActivity.class);
        intent.putExtra("layout", R.layout.walk1);
        startActivity(intent);
        
        mapButton = (Button)findViewById(R.id.mapButton);
        mapButton.setOnClickListener(mapClick);
        
        modeToggle = (ToggleButton)findViewById(R.id.toggleButton1);
        modeToggle.setText("View pictures");
        modeToggle.setTextOn("Choose pictures");
        modeToggle.setTextOff("View pictures");
        modeToggle.setOnCheckedChangeListener(modeChanged);
        
        Vector<ImageCategory> pics = new Vector<ImageCategory>();
        ImageCategory cat1 = new ImageCategory("kategoria 1");
        pics.add(cat1);
        cat1.add(R.drawable.icon1);
        cat1.add(R.drawable.pic_typo1);
        cat1.add(R.drawable.pic_typo2);
        cat1.add(R.drawable.pic_typo3);
        cat1.add(R.drawable.pic_typo4);
        cat1.add(R.drawable.pic_typo5);
        
        ImageCategory cat2 = new ImageCategory("kategoria 2");
        pics.add(cat2);
        cat2.add(R.drawable.icon2);
        cat2.add(R.drawable.pic_mural4);
        cat2.add(R.drawable.pic_mural2);
        cat2.add(R.drawable.pic_mural3);
        cat2.add(R.drawable.pic_mural1);
        
        ImageCategory cat3 = new ImageCategory("kategoria 3");
        pics.add(cat3);
        cat3.add(R.drawable.icon3);
        cat3.add(R.drawable.pic_archi1);
        cat3.add(R.drawable.pic_archi2);
        cat3.add(R.drawable.pic_archi3);
        cat3.add(R.drawable.pic_archi4);
        
        ImageCategory cat4 = new ImageCategory("kategoria 4");
        pics.add(cat4);
        cat4.add(R.drawable.icon4);
        cat4.add(R.drawable.img6);
        cat4.add(R.drawable.img7);
        cat4.add(R.drawable.img5);
        
        buildImageList(pics, 240);
    }
    
    public void buildImageList(Vector<ImageCategory> categories, Integer rowSize) {
    	LinearLayout topLevel = (LinearLayout)findViewById(R.id.pic_layout);
    	
    	ScrollView vertical = new ScrollView(this);
    	topLevel.addView(vertical, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    	
    	LinearLayout LL1 = new LinearLayout(this);
    	LL1.setOrientation(LinearLayout.VERTICAL);
    	vertical.addView(LL1, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    	
    	for (ImageCategory category : categories) {
    		//ROW
        	HorizontalScrollView horizontal = new HorizontalScrollView(this);
        	//  android:scrollbars="horizontal"
        	LL1.addView(horizontal, new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.MATCH_PARENT));
        	// 150dp
        	
        	LinearLayout LL2 = new LinearLayout(this);
        	LL2.setOrientation(LinearLayout.HORIZONTAL);
        	horizontal.addView(LL2, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, rowSize));
        	// android:layout_height="150dp"
        	
        	for (Integer picResId : category.pics) {
        		
        		ImageView im = new ImageView(this);
            	im.setImageResource(picResId);
            	im.setAdjustViewBounds(true);
            	
            	//clickable
            	im.setClickable(true);
            	im.setOnClickListener(imageClick);
            	im.setTag(R.id.pic_layout, category.name);
            	im.setTag(R.id.big_image_view, picResId);
            	
            	images.add(im);
            	LL2.addView(im, new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT));
        	}
       
    	}

    	
    }
    
    private OnClickListener mapClick = new OnClickListener() {
		public void onClick(View v) {
			Intent intent = new Intent(CategoryViewActivity.this, MyMapActivity.class);
			startActivity(intent);
			
		}
	};

    private OnCheckedChangeListener modeChanged = new OnCheckedChangeListener() {
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			Log.d("mode", "checked");
			editMode = isChecked;
			if (editMode == false) {
				for(ImageView img : images) {
					img.setAlpha(255);
					img.invalidate();
				}
			}
		}
	};
    
	private OnClickListener imageClick = new OnClickListener() {
		public void onClick(View v) {
			ImageView me = (ImageView)v;
			
			if (editMode) {
				me.setAlpha(100);
				me.invalidate();
			} else {
				Integer picResId = (Integer)me.getTag(R.id.big_image_view);
				Intent intent = new Intent(CategoryViewActivity.this, FullViewActivity.class);
				intent.putExtra("imgResId", picResId);
				startActivity(intent);
			}

		}
	};
    
}