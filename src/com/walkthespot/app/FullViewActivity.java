package com.walkthespot.app;

import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class FullViewActivity extends Activity {
	
	ImageView imageView;
	Button shareButton;
	Button mapButton;
	Button infoButton;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_view);
        shareButton = (Button)findViewById(R.id.share_button);
        shareButton.setOnClickListener(shareClick);
        
        mapButton = (Button)findViewById(R.id.map_button);
        mapButton.setOnClickListener(mapClick);
        
        infoButton = (Button)findViewById(R.id.info_button);
        infoButton.setOnClickListener(infoClick);
    }

	@Override
	protected void onResume() {
		super.onResume();
		Bundle b = getIntent().getExtras();
		
		Integer imgResId = b.getInt("imgResId");
		Drawable imgDrawable = (Drawable)b.get("imgDrawable");
		
		imageView = (ImageView) findViewById(R.id.big_image_view);
		if (imgResId != 0) {
			imageView.setImageResource(imgResId);
		} else if (imgDrawable != null) {
			imageView.setImageDrawable(imgDrawable);
		}
		
	}
	
	OnClickListener shareClick = new OnClickListener() {
		public void onClick(View v) {
			final CharSequence[] items = {"Facebook", "Reddit", "Digg"};

			AlertDialog.Builder builder = new AlertDialog.Builder(FullViewActivity.this);
			builder.setTitle("Share");
			builder.setItems(items, new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog, int item) {
			        Toast.makeText(getApplicationContext(), items[item], Toast.LENGTH_SHORT).show();
			    }
			});
			AlertDialog alert = builder.create();
			alert.show();
		}
	};
	
	OnClickListener mapClick = new OnClickListener() {
		public void onClick(View v) {
			Intent intent = new Intent(FullViewActivity.this, MyMapActivity.class);
			startActivity(intent);
		}
	};
	
	OnClickListener infoClick = new OnClickListener() {
		public void onClick(View v) {
			Dialog dialog = new Dialog(FullViewActivity.this);

			dialog.setContentView(R.layout.custom_dialog);
			dialog.setTitle("Photo info");

			TextView text = (TextView) dialog.findViewById(R.id.text);
			text.setText("Some info!");
			dialog.show();
		}
	};

}