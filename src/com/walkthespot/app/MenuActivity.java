package com.walkthespot.app;

import java.util.Vector;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CompoundButton;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ToggleButton;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;

public class MenuActivity extends Activity {
    /** Called when the activity is first created. */
	ImageView op1;
	ImageView op2;
	ImageView op3;
	ImageView op4;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu2_view);
        
        op1 = (ImageView)findViewById(R.id.imageView1);
        op2 = (ImageView)findViewById(R.id.imageView2);
        op3 = (ImageView)findViewById(R.id.imageView3);
        op4 = (ImageView)findViewById(R.id.imageView4);
        
        // WALKTHROUGH
        //Intent intent = new Intent(this, WalkthroughActivity.class);
        //intent.putExtra("layout", R.layout.walk2);
        //startActivity(intent);
        
        op1.setOnClickListener(imageClick);
        op2.setOnClickListener(imageClick);
        op3.setOnClickListener(imageClick);
        op4.setOnClickListener(imageClick);
    }
    

	private OnClickListener imageClick = new OnClickListener() {
		public void onClick(View v) {
			Intent intent = new Intent(MenuActivity.this, CategoryViewActivity.class);
	        startActivity(intent);
		}
	};
    
}