package com.walkthespot.app;

import java.util.List;
import java.util.Vector;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

public class MyMapActivity extends MapActivity {
	
	MapView mapView;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.map_view);
	    
	    mapView = (MapView) findViewById(R.id.mapview);
	    mapView.setBuiltInZoomControls(true);
	    
	    MapController mc = mapView.getController();
	    double lat = 50.062429;
	    double lon = 19.937611;
	    GeoPoint location = new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6));
	    mc.animateTo(location);
	    mc.setZoom(16);
	    
	    List<Overlay> mapOverlays = mapView.getOverlays();
	    Drawable drawable = this.getResources().getDrawable(R.drawable.androidmarker);
	    HelloItemizedOverlay itemizedoverlay = new HelloItemizedOverlay(drawable, this);
	    
	    GeoPoint point = new GeoPoint((int) ((lat + 0.003) * 1E6), (int) ((lon + 0.0003) * 1E6));
	    OverlayItem overlayitem = new OverlayItem(point, "Hola, Mundo!", "I'm in Mexico City!");
	    itemizedoverlay.addOverlay(overlayitem);
	    
	    GeoPoint point2 = new GeoPoint((int) ((lat+ 0.001) * 1E6 ), (int) ((lon + 0.002) * 1E6));
	    OverlayItem overlayitem2 = new OverlayItem(point2, "Hola, Mundo!", "I'm in Mexico City!");
	    itemizedoverlay.addOverlay(overlayitem2);
	    
	    GeoPoint point3 = new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6));
	    OverlayItem overlayitem3 = new OverlayItem(point3, "Hola, Mundo!", "I'm in Mexico City!");
	    itemizedoverlay.addOverlay(overlayitem3);
	    
	    
	    mapOverlays.add(itemizedoverlay);
	    
	    /*
	    Vector<GeoPoint> geoPoints = new Vector<GeoPoint>();
	    geoPoints.add(new GeoPoint((int) ((lat + 0.003) * 1E6), (int) ((lon + 0.0003) * 1E6)));
	    geoPoints.add(new GeoPoint((int) ((lat+ 0.001) * 1E6 ), (int) ((lon + 0.002) * 1E6)));
	    geoPoints.add(new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6)));
	    drawPath(geoPoints, Color.BLACK);
	    */
	}
	
	private void drawPath(List<GeoPoint> geoPoints, int color) {
	   List<Overlay> overlays = mapView.getOverlays();
	   for (int i = 1; i < geoPoints.size(); i++) {
		   overlays.add(new RouteOverlay(geoPoints.get(i - 1), geoPoints.get(i), color));
	   }
	}
	
	
	@Override
	protected boolean isRouteDisplayed() {
	    return false;
	}
}